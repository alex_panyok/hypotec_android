package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable


class CalcQuizItem(title: String, answerItem: AnswerItem? = null, canSkip: Boolean = false, key: String, val keys: List<String>) : QuizItem(title, answerItem, canSkip, key), Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readParcelable<AnswerItem>(AnswerItem::class.java.classLoader),
            1 == source.readInt(),
            source.readString(),
            source.createStringArrayList()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeParcelable(answerItem, 0)
        writeInt((if (canSkip) 1 else 0))
        writeString(key)
        writeStringList(keys)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CalcQuizItem> = object : Parcelable.Creator<CalcQuizItem> {
            override fun createFromParcel(source: Parcel): CalcQuizItem = CalcQuizItem(source)
            override fun newArray(size: Int): Array<CalcQuizItem?> = arrayOfNulls(size)
        }
    }
}