package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.LoanOptions


class SimpleAnswerItem(score: Int,
                       reset: Boolean = false,
                       options: List<LoanOptions>,
                       val skipStep: Int = -1,
                       val title: String) : AnswerItem(score, reset, options), Parcelable {
    override fun getAnswerValue(): String {
        return title
    }

    constructor(source: Parcel) : this(
            source.readInt(),
            1 == source.readInt(),
            ArrayList<LoanOptions>().apply { source.readList(this, LoanOptions::class.java.classLoader) },
            source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(score)
        writeInt((if (reset) 1 else 0))
        writeList(list)
        writeInt(skipStep)
        writeString(title)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SimpleAnswerItem> = object : Parcelable.Creator<SimpleAnswerItem> {
            override fun createFromParcel(source: Parcel): SimpleAnswerItem = SimpleAnswerItem(source)
            override fun newArray(size: Int): Array<SimpleAnswerItem?> = arrayOfNulls(size)
        }
    }
}