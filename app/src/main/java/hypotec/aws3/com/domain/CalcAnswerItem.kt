package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.LoanOptions


class CalcAnswerItem(score: Int,
                     reset: Boolean = false,
                     list: List<LoanOptions>,
                     val answers: List<String>) : AnswerItem(score, reset, list), Parcelable {
    override fun getAnswerValue(): String {
        return ""
    }

    constructor(source: Parcel) : this(
            source.readInt(),
            1 == source.readInt(),
            ArrayList<LoanOptions>().apply { source.readList(this, LoanOptions::class.java.classLoader) },
            source.createStringArrayList()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(score)
        writeInt((if (reset) 1 else 0))
        writeList(list)
        writeStringList(answers)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CalcAnswerItem> = object : Parcelable.Creator<CalcAnswerItem> {
            override fun createFromParcel(source: Parcel): CalcAnswerItem = CalcAnswerItem(source)
            override fun newArray(size: Int): Array<CalcAnswerItem?> = arrayOfNulls(size)
        }
    }
}