package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.LoanOptions


class QuizResult(val result: Map<String, String>, val recommended: LoanOptions) : Parcelable {
    constructor(source: Parcel) : this(
            HashMap<String, String>().apply {
                (0 until size).forEach {
                    val key = source.readString()
                    val value = source.readString()
                    this.put(key, value) }
            },
            LoanOptions.values()[source.readInt()]
    )


    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(result.size)
        for (entry in result.entries) {
            writeString(entry.key)
            writeString(entry.value)
        }
        writeInt(recommended.ordinal)

    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<QuizResult> = object : Parcelable.Creator<QuizResult> {
            override fun createFromParcel(source: Parcel): QuizResult = QuizResult(source)
            override fun newArray(size: Int): Array<QuizResult?> = arrayOfNulls(size)
        }
    }
}