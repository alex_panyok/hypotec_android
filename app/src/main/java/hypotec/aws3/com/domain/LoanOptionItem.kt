package hypotec.aws3.com.domain

import hypotec.aws3.com.enums.LoanOptions
import hypotec.aws3.com.util.AdapterKeys
import tdd.aws3.com.tddexample.view.ViewType


data class LoanOptionItem(val loanOptions: LoanOptions, var index:Int = 0, val recommended:Boolean):ViewType{
    override fun getViewType(): Int {
       return AdapterKeys.LOAN_OPTION
    }

}