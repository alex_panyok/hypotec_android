package hypotec.aws3.com.domain.repo

import hypotec.aws3.com.domain.QuizItem
import hypotec.aws3.com.domain.api.response.BaseResponse
import hypotec.aws3.com.enums.Quiz
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response


interface QuizRepo {

    fun getQuiz(quiz:Quiz): Observable<List<QuizItem>>

    fun send(map: Map<String,String>):Observable<BaseResponse>
}