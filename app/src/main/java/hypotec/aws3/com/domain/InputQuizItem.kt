package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.InputType


class InputQuizItem(title: String, answerItem: AnswerItem? = null, canSkip: Boolean = false,  key:String, val hint: String, val subHint: String? = null, val inputType: InputType) : QuizItem(title, answerItem, canSkip, key), Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readParcelable<AnswerItem>(AnswerItem::class.java.classLoader),
            source.readValue(Boolean::class.java.classLoader) as Boolean,
            source.readString(),
            source.readString(),
            source.readString(),
            InputType.values()[source.readInt()]
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeParcelable(answerItem, 0)
        writeValue(canSkip)
        writeString(key)
        writeString(hint)
        writeString(subHint)
        writeInt(inputType.ordinal)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<InputQuizItem> = object : Parcelable.Creator<InputQuizItem> {
            override fun createFromParcel(source: Parcel): InputQuizItem = InputQuizItem(source)
            override fun newArray(size: Int): Array<InputQuizItem?> = arrayOfNulls(size)
        }
    }
}