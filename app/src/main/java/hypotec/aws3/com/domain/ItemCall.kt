package hypotec.aws3.com.domain

import hypotec.aws3.com.util.AdapterKeys
import tdd.aws3.com.tddexample.view.ViewType


class ItemCall:ViewType {
    override fun getViewType(): Int {
        return AdapterKeys.CALL_ITEM
    }
}