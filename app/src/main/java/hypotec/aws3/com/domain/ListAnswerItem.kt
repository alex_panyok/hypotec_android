package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.LoanOptions
import hypotec.aws3.com.util.AdapterKeys
import tdd.aws3.com.tddexample.view.ViewType
import java.util.*


class ListAnswerItem(score: Int,
                     reset: Boolean = false,
                     options: List<LoanOptions>,
                     val title: String,
                     val subTitle: String? = null,
                     var isActivated: Boolean? = false) : AnswerItem(score, reset, options), ViewType, Parcelable {
    override fun getViewType(): Int {
        return AdapterKeys.LIST_ANSWER
    }


    override fun getAnswerValue(): String {
        return title
    }





    constructor(source: Parcel) : this(
            source.readInt(),
            1 == source.readInt(),
            ArrayList<LoanOptions>().apply { source.readList(this, LoanOptions::class.java.classLoader) },
            source.readString(),
            source.readString(),
            source.readValue(Boolean::class.java.classLoader) as Boolean?
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(score)
        writeInt((if (reset) 1 else 0))

        writeList(list)
        writeString(title)
        writeString(subTitle)
        writeValue(isActivated)
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ListAnswerItem

        if (title != other.title) return false

        return true
    }

    override fun hashCode(): Int {
        return title.hashCode()
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ListAnswerItem> = object : Parcelable.Creator<ListAnswerItem> {
            override fun createFromParcel(source: Parcel): ListAnswerItem = ListAnswerItem(source)
            override fun newArray(size: Int): Array<ListAnswerItem?> = arrayOfNulls(size)
        }
    }
}