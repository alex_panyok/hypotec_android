package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable


class ListQuizItem(title: String, answerItem: AnswerItem? = null, canSkip: Boolean = false, key:String, val answers: List<AnswerItem>) : QuizItem(title, answerItem, canSkip, key), Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readParcelable<AnswerItem>(AnswerItem::class.java.classLoader),
            source.readValue(Boolean::class.java.classLoader) as Boolean,
            source.readString(),
            source.createTypedArrayList(AnswerItem.CREATOR)
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeParcelable(answerItem, 0)
        writeValue(canSkip)
        writeString(key)
        writeTypedList(answers)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<ListQuizItem> = object : Parcelable.Creator<ListQuizItem> {
            override fun createFromParcel(source: Parcel): ListQuizItem = ListQuizItem(source)
            override fun newArray(size: Int): Array<ListQuizItem?> = arrayOfNulls(size)
        }
    }
}