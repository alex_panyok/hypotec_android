package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.LoanOptions
import tdd.aws3.com.tddexample.view.ViewType
import java.util.AbstractMap


open class AnswerItem(val score: Int, val reset: Boolean = false,  val list: List<LoanOptions>) : Parcelable, ViewType {
    override fun getViewType(): Int {
        return -1
    }

    open fun getAnswerValue(): String {
        return ""
    }

    constructor(source: Parcel) : this(
            source.readInt(),
            1 == source.readInt(),
            ArrayList<LoanOptions>().apply { source.readList(this, LoanOptions::class.java.classLoader) }
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(score)
        writeInt((if (reset) 1 else 0))
        writeList(list)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<AnswerItem> = object : Parcelable.Creator<AnswerItem> {
            override fun createFromParcel(source: Parcel): AnswerItem = AnswerItem(source)
            override fun newArray(size: Int): Array<AnswerItem?> = arrayOfNulls(size)
        }
    }


}