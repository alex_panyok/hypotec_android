package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable
import hypotec.aws3.com.enums.LoanOptions
import java.util.AbstractMap
import java.util.ArrayList


class InputAnswerItem(score: Int,
                      reset: Boolean = false,

                      options: List<LoanOptions>,
                      val title: String) : AnswerItem(score, reset,  options), Parcelable {
    override fun getAnswerValue(): String {
        return title
    }

    constructor(source: Parcel) : this(
            source.readInt(),
            1 == source.readInt(),
            ArrayList<LoanOptions>().apply { source.readList(this, LoanOptions::class.java.classLoader) },
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(score)
        writeInt((if (reset) 1 else 0))
        writeList(list)
        writeString(title)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<InputAnswerItem> = object : Parcelable.Creator<InputAnswerItem> {
            override fun createFromParcel(source: Parcel): InputAnswerItem = InputAnswerItem(source)
            override fun newArray(size: Int): Array<InputAnswerItem?> = arrayOfNulls(size)
        }
    }
}