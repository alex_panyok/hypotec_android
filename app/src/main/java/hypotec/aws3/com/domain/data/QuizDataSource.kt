package hypotec.aws3.com.domain.data

import android.content.Context
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.*
import hypotec.aws3.com.enums.InputType
import hypotec.aws3.com.enums.LoanOptions
import hypotec.aws3.com.enums.Quiz
import hypotec.aws3.com.util.QuizKeys
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class QuizDataSource @Inject constructor(context: Context) : DataSource {


    val res = context.resources!!


    override fun getQuiz(quiz: Quiz): Observable<List<QuizItem>> {
        return when (quiz) {
            Quiz.BUY_HOME -> Observable.just(getBuyHomeQuiz())
            Quiz.REFINANCE -> Observable.just(getRefinanceQuiz())
        }
    }


    private fun getRefinanceQuiz(): List<QuizItem> {
        val listRefinance = ArrayList<QuizItem>()


        listRefinance.add(ListQuizItem(res.getString(R.string.quiz1_2), canSkip = true, key = QuizKeys.STEP_DATA_ST_1, answers =
        listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz1_2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                ListAnswerItem(1, title = res.getString(R.string.answer2_quiz1_2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                ListAnswerItem(1, title = res.getString(R.string.answer3_quiz1_2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                ListAnswerItem(1, title = res.getString(R.string.answer4_quiz1_2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)))))


        listRefinance.add(InputQuizItem(res.getString(R.string.quiz2_2), hint = "", key = QuizKeys.STEP_DATA_ST_2_M_B, inputType = InputType.US_DOLLAR))



        listRefinance.add(SimpleQuizItem(res.getString(R.string.quiz3_2),
                key = QuizKeys.STEP_DATA_ST_3,
                answers =
                Pair(SimpleAnswerItem(1, options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV), title = res.getString(R.string.yes)),
                        SimpleAnswerItem(1, options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV), title = res.getString(R.string.no), skipStep = 3))))


        listRefinance.add(SimpleQuizItem(res.getString(R.string.quiz4_2),
                key = QuizKeys.STEP_DATA_ST_4,
                answers =
                Pair(SimpleAnswerItem(1, options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV), title = res.getString(R.string.yes)),
                        SimpleAnswerItem(1, options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV), title = res.getString(R.string.no)))))



        listRefinance.add(InputQuizItem(res.getString(R.string.quiz6_2),
                key = QuizKeys.STEP_DATA_ST_5_C_A,
                hint = "",
                subHint = res.getString(R.string.cash_out),
                inputType = InputType.US_DOLLAR_NOT_REQUIRE))


        listRefinance.add(ListQuizItem(res.getString(R.string.quiz4),
                canSkip = false,
                key = QuizKeys.STEP_DATA_ST_6,
                answers = listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer2_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer3_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer4_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)))))


        listRefinance.add(SimpleQuizItem(res.getString(R.string.quiz7),
                key = QuizKeys.STEP_DATA_ST_7,
                answers =
                Pair(SimpleAnswerItem(3, options = listOf(LoanOptions.VA), title = res.getString(R.string.yes)),
                        SimpleAnswerItem(3, options = listOf(LoanOptions.FHA, LoanOptions.CONV), title = res.getString(R.string.no)))))


        listRefinance.add(ListQuizItem(res.getString(R.string.quiz8),
                key = QuizKeys.STEP_DATA_ST_8,
                answers =
                listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz8), subTitle = res.getString(R.string.answer_sub1_quiz8), options = listOf(LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer2_quiz8), subTitle = res.getString(R.string.answer_sub2_quiz8), options = listOf(LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer3_quiz8), subTitle = res.getString(R.string.answer_sub3_quiz8), options = listOf(LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer4_quiz8), subTitle = res.getString(R.string.answer_sub4_quiz8), options = listOf(LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer5_quiz8), subTitle = res.getString(R.string.answer_sub5_quiz8), reset = true, options = listOf(LoanOptions.CONV)))))


        return listRefinance
    }


    private fun getBuyHomeQuiz(): List<QuizItem> {
        val listBuy = ArrayList<QuizItem>()


        listBuy.add(ListQuizItem(res.getString(R.string.quiz1),
                key = QuizKeys.STEP_DATA_ST_1,
                answers =
                listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz1), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer2_quiz1), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer3_quiz1), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)))))


        listBuy.add(ListQuizItem(res.getString(R.string.quiz2),
                key = QuizKeys.STEP_DATA_ST_2,
                answers =
                listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer2_quiz2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer3_quiz2), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)))))


        listBuy.add(InputQuizItem(res.getString(R.string.quiz3),
                key = QuizKeys.STEP_DATA_ST_3,
                hint = res.getString(R.string.zip_code_hint),
                inputType = InputType.ZIP_CODE))


        listBuy.add(ListQuizItem(res.getString(R.string.quiz4),
                canSkip = false,
                key = QuizKeys.STEP_DATA_ST_4,
                answers =
                listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer2_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer3_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer4_quiz4), subTitle = res.getString(R.string.answer_sub_quiz4), options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV)))))


        listBuy.add(CalcQuizItem(title = res.getString(R.string.quiz5), key = "",
                keys = listOf(QuizKeys.STEP_DATA_ST_5_F_P,
                        QuizKeys.STEP_DATA_ST_5_D_P, QuizKeys.STEP_DATA_ST_5_D_P_P)))


        listBuy.add(SimpleQuizItem(res.getString(R.string.quiz6),
                key = QuizKeys.STEP_DATA_ST_6,
                answers =
                Pair(SimpleAnswerItem(1, options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV), title = res.getString(R.string.yes)),
                        SimpleAnswerItem(1, options = listOf(LoanOptions.FHA, LoanOptions.VA, LoanOptions.CONV), title = res.getString(R.string.no)))))

        listBuy.add(SimpleQuizItem(res.getString(R.string.quiz7),
                key = QuizKeys.STEP_DATA_ST_7,
                answers =
                Pair(SimpleAnswerItem(3, options = listOf(LoanOptions.VA), title = res.getString(R.string.yes)),
                        SimpleAnswerItem(3, options = listOf(LoanOptions.FHA, LoanOptions.CONV), title = res.getString(R.string.no)))))


        listBuy.add(ListQuizItem(res.getString(R.string.quiz8),
                key = QuizKeys.STEP_DATA_ST_8,
                answers =
                listOf(ListAnswerItem(1, title = res.getString(R.string.answer1_quiz8), subTitle = res.getString(R.string.answer_sub1_quiz8), options = listOf(LoanOptions.FHA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer2_quiz8), subTitle = res.getString(R.string.answer_sub2_quiz8), options = listOf(LoanOptions.FHA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer3_quiz8), subTitle = res.getString(R.string.answer_sub3_quiz8), options = listOf(LoanOptions.FHA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer4_quiz8), subTitle = res.getString(R.string.answer_sub4_quiz8), options = listOf(LoanOptions.FHA, LoanOptions.CONV)),
                        ListAnswerItem(1, title = res.getString(R.string.answer5_quiz8), subTitle = res.getString(R.string.answer_sub5_quiz8), reset = true, options = listOf(LoanOptions.FHA, LoanOptions.CONV)))))
        return listBuy
    }


}