package hypotec.aws3.com.domain.data

import hypotec.aws3.com.domain.QuizItem
import hypotec.aws3.com.enums.Quiz
import io.reactivex.Observable
import java.util.*


interface DataSource {

    fun getQuiz(quiz:Quiz): Observable<List<QuizItem>>
}