package hypotec.aws3.com.domain

import android.os.Parcelable


abstract class QuizItem(val title:String, var answerItem: AnswerItem?, val canSkip:Boolean = false, val key:String):Parcelable