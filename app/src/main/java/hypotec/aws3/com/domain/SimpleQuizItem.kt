package hypotec.aws3.com.domain

import android.os.Parcel
import android.os.Parcelable

class SimpleQuizItem(title: String, answerItem: AnswerItem? = null, canSkip: Boolean = false, key:String,  val answers: Pair<AnswerItem, AnswerItem>) : QuizItem(title, answerItem, canSkip, key), Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readParcelable<AnswerItem>(AnswerItem::class.java.classLoader),
            1 == source.readInt(),
            source.readString(),
            Pair(source.readParcelable<AnswerItem>(AnswerItem::class.java.classLoader),
                    source.readParcelable<AnswerItem>(AnswerItem::class.java.classLoader))
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(title)
        writeParcelable(answerItem, 0)
        writeInt((if (canSkip) 1 else 0))
        writeString(key)
        writeParcelable(answers.first, 0)
        writeParcelable(answers.first, 0)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SimpleQuizItem> = object : Parcelable.Creator<SimpleQuizItem> {
            override fun createFromParcel(source: Parcel): SimpleQuizItem = SimpleQuizItem(source)
            override fun newArray(size: Int): Array<SimpleQuizItem?> = arrayOfNulls(size)
        }
    }
}