package hypotec.aws3.com.domain.repo

import hypotec.aws3.com.domain.QuizItem
import hypotec.aws3.com.domain.api.ApiService
import hypotec.aws3.com.domain.api.response.BaseResponse
import hypotec.aws3.com.domain.data.DataSource
import hypotec.aws3.com.domain.data.QuizDataSource
import hypotec.aws3.com.enums.Quiz
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.ResponseBody
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class QuizRepoImpl @Inject constructor(private val quizDataSource: DataSource, private val apiService: ApiService) : QuizRepo {
    override fun send(map: Map<String, String>):Observable<BaseResponse> {
        return apiService.send(map)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())

    }

    override fun getQuiz(quiz: Quiz): Observable<List<QuizItem>> {
        return quizDataSource.getQuiz(quiz)
    }




}