package hypotec.aws3.com.domain.api

import hypotec.aws3.com.domain.api.response.BaseResponse
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.FieldMap
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST


interface ApiService {

    @FormUrlEncoded
    @POST("/wp-admin/admin-ajax.php")
    fun send(@FieldMap map: Map<String, String>): Observable<BaseResponse>
}