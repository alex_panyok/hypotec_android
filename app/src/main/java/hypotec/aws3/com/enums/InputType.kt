package hypotec.aws3.com.enums


enum class InputType {
    ZIP_CODE,
    US_DOLLAR,
    US_DOLLAR_NOT_REQUIRE
}