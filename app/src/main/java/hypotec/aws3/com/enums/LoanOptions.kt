package hypotec.aws3.com.enums


enum class LoanOptions(val title:String) {

    FHA("FHA loan"),
    VA("VA loan"),
    CONV("Conventional Loan"),
    ANY("")


}