package hypotec.aws3.com.view.fragment

import android.os.Bundle
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.support.v4.app.DialogFragment
import android.telephony.PhoneNumberFormattingTextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import com.mobsandgeeks.saripaar.annotation.NotEmpty
import hypotec.aws3.com.R
import hypotec.aws3.com.enums.LoanOptions
import hypotec.aws3.com.util.Keys
import hypotec.aws3.com.util.TextsUtils
import hypotec.aws3.com.validation.QuickRuleEmail
import hypotec.aws3.com.validation.QuickRulePhone


class DialogApply : DialogFragment(), Validator.ValidationListener {

    @NotEmpty
    @BindView(R.id.etName)
    lateinit var etName: TextInputEditText

    @NotEmpty
    @BindView(R.id.etSurname)
    lateinit var etSurname: TextInputEditText

    @BindView(R.id.etEmail)
    lateinit var etEmail: TextInputEditText

    @BindView(R.id.etPhone)
    lateinit var etPhone: TextInputEditText

    @OnClick(R.id.close)
    fun close() {
        dismiss()
    }

    @OnClick(R.id.submit)
    fun submit() {
        TextsUtils.clearFocus(activity?.applicationContext, etName, etSurname, etEmail, etPhone)
        validator.validate()
    }

    companion object {
        fun getInstance(option: LoanOptions): DialogApply {
            return DialogApply().apply {
                arguments = Bundle().apply { putSerializable(Keys.ITEM, option) }
            }
        }
    }


    private val validator = Validator(this).apply {
        setValidationListener(this@DialogApply)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_apply, container, false)
        ButterKnife.bind(this, v)
        validator.apply {
            put(etEmail, QuickRuleEmail())
            put(etPhone, QuickRulePhone())
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return v
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        TextsUtils.setFocusListeners(etName, etSurname, etEmail, etPhone)
        etPhone.addTextChangedListener(PhoneNumberFormattingTextWatcher())
    }


    override fun onResume() {
        super.onResume()
        if (dialog.window != null)
            dialog.window.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    override fun onValidationFailed(errors: List<ValidationError>) {
        for (error in errors) {
            val view = error.view
            val message = error.getCollatedErrorMessage(activity)
            if (view is TextInputEditText) {
                setErrorEditText(view, message)
            }
        }
    }


    private fun setErrorEditText(editText: TextInputEditText?, message: String) {
        if (editText == null)
            return
        var parent = editText.parent
        while (parent is View) {
            if (parent is TextInputLayout) {
                parent.error = message
                break
            }
            parent = parent.getParent()
        }
    }

    override fun onValidationSucceeded() {
        if (parentFragment != null && parentFragment is QuizResultFragment) {
            (parentFragment as QuizResultFragment).send(etName.text.toString(), etSurname.text.toString(), etEmail.text.toString(), etPhone.text.toString(), arguments?.getSerializable(Keys.ITEM) as LoanOptions)
        }
        dismiss()
    }

}