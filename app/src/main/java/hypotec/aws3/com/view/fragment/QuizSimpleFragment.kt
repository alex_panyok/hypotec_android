package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.SimpleQuizItem
import hypotec.aws3.com.util.inflate
import hypotec.aws3.com.viewmodel.QuizSimpleModel
import kotlinx.android.synthetic.main.fragment_quiz.*


class QuizSimpleFragment : QuizFragment<SimpleQuizItem, QuizSimpleModel>() {


    override fun initViews() {
        yes.setOnClickListener {
            viewModel.setAnswer(true)
            next()
        }
        no.setOnClickListener {
            viewModel.setAnswer(false)
            next()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_quiz)
    }



    companion object {
        fun create(bundle: Bundle): QuizSimpleFragment {
            val fragment = QuizSimpleFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}