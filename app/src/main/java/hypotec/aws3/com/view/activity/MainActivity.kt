package hypotec.aws3.com.view.activity

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import hypotec.aws3.com.R
import hypotec.aws3.com.enums.Quiz
import hypotec.aws3.com.util.Keys
import hypotec.aws3.com.util.launchActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity: AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        buyHome.setOnClickListener {
            launchActivity<QuizActivity> { putExtra(Keys.QUIZ, Quiz.BUY_HOME) }
        }
        refinance.setOnClickListener{
            launchActivity<QuizActivity> { putExtra(Keys.QUIZ, Quiz.REFINANCE) }
        }
        calculator.setOnClickListener{
            launchActivity<CalculatorActivity> {
            }
        }

        contacts.setOnClickListener {
            launchActivity<ContactActivity> {  }
        }



    }
}