package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import java.lang.reflect.ParameterizedType
import javax.inject.Inject

abstract class BaseVMFragment<VM : ViewModel> : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var viewModel: VM

    private fun createViewModel() {


        viewModel = try {
            ViewModelProviders.of(this, viewModelFactory).
                    get((javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<VM>)

        } catch (e: IllegalArgumentException) {
            ViewModelProviders.of(this, viewModelFactory).
                    get((javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[1] as Class<VM>)
        }


    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        createViewModel()

    }

}