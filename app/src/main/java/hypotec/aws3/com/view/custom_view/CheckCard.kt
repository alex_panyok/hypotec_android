package hypotec.aws3.com.view.custom_view

import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.CardView
import android.util.AttributeSet
import android.view.Gravity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import hypotec.aws3.com.R
import hypotec.aws3.com.util.PixelUtil


class CheckCard (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : FrameLayout(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

    private val container  = FrameLayout(context)

    private val titleContainer  = LinearLayout(context).apply { orientation = LinearLayout.VERTICAL }

    private val titleTextView = TextView(context)


    private val checkBoxImage = ImageView(context).apply { setImageResource(R.drawable.ic_check_on) }

    private val subTitleTextView = TextView(context)



    init{
        initViews()
        checkActivated()
    }


    private fun initViews(){
        addContainer()
        addTitleContainer()
        addTitle()
        addCheckBoxImageView()
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = (PixelUtil.getScreenWidth(context)*0.6).toInt()
        val height = (width*0.5).toInt()
        super.onMeasure(MeasureSpec.makeMeasureSpec( width, MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY))
    }

    private fun addTitle(){
        titleTextView.setTextColor(Color.BLACK)
        titleTextView.setTypeface(titleTextView.typeface, Typeface.BOLD)
        titleTextView.gravity = Gravity.CENTER
        titleContainer.addView(titleTextView, LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT))
    }



    private fun addSubTitle(){
        if(subTitleTextView.parent==null) {
            subTitleTextView.setTextColor(ContextCompat.getColor(context, R.color.colorHint))
            subTitleTextView.setTypeface(titleTextView.typeface, Typeface.BOLD)
            subTitleTextView.gravity = Gravity.CENTER
            titleContainer.addView(subTitleTextView, LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT))
        }
    }

    private fun addTitleContainer(){
        container.addView(titleContainer.apply { gravity = Gravity.CENTER }, FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT).apply {
            gravity = Gravity.CENTER
        })
    }


    fun setText(title:String, subTitle:String? = null){
        titleTextView.text = title
        if(subTitle!=null){
            addSubTitle()
            subTitleTextView.text= subTitle
        }
    }


    private fun addCheckBoxImageView(){
        container.addView(checkBoxImage, FrameLayout.LayoutParams(context.resources.getDimensionPixelSize(R.dimen.checkBoxSize),
                context.resources.getDimensionPixelSize(R.dimen.checkBoxSize)).apply { gravity = Gravity.END })
    }

    private fun addContainer(){
        addView(container, FrameLayout.LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT))
    }

    override fun setActivated(activated: Boolean) {
        super.setActivated(activated)
        checkActivated()
    }


    private fun checkActivated(){
        if(isActivated) {
            checkBoxImage.visibility = View.VISIBLE
            titleTextView.setTextColor(Color.WHITE)
            setBackgroundColor(ContextCompat.getColor(context, R.color.colorAccent))
        }else {
            checkBoxImage.visibility = View.INVISIBLE
            titleTextView.setTextColor(Color.BLACK )
            setBackgroundColor(Color.WHITE)
        }
    }
}