package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.LoanOptionItem
import hypotec.aws3.com.domain.QuizResult
import hypotec.aws3.com.enums.LoanOptions
import hypotec.aws3.com.util.Keys
import hypotec.aws3.com.util.QuizKeys
import hypotec.aws3.com.util.inflate
import hypotec.aws3.com.util.startActionPhone
import hypotec.aws3.com.validation.QuickRulePhone
import hypotec.aws3.com.view.activity.QuizActivity
import hypotec.aws3.com.view.adapter.AdapterLoanOption
import hypotec.aws3.com.viewmodel.QuizResultModel
import kotlinx.android.synthetic.main.fragment_result_quiz.*
import kotlinx.android.synthetic.main.item_call.*
import kotlinx.android.synthetic.main.layout_fragment_title.*
import kotlinx.android.synthetic.main.layout_no_recommended.*


class QuizResultFragment : BaseVMFragment<QuizResultModel>(), AdapterLoanOption.OnAdapterLoanOptionListener {


    private val adapterLoanOption = AdapterLoanOption(this)

    private  var quizResult:QuizResult? = null

    override fun initViews() {
        titleFragment.text = getString(R.string.available_loan_options)
        rv.apply {
            layoutManager = GridLayoutManager(activity, 2, GridLayoutManager.VERTICAL, false)
            adapter = adapterLoanOption
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_result_quiz)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        quizResult = arguments?.getParcelable(Keys.ITEM)
        viewModel.liveData.observe(this, Observer {
            if (it != null) {
                if (!it.isEmpty()) {
                    noOption.visibility = View.GONE
                    adapterLoanOption.setItems(it)
                } else
                    noOption.visibility = View.VISIBLE
            }
        })
        viewModel.setResult(arguments?.getParcelable(Keys.ITEM))
        root.setOnClickListener {
            activity?.startActionPhone(getString(R.string.raw_phone))
        }
    }

    companion object {
        fun create(bundle: Bundle): QuizResultFragment {
            val fragment = QuizResultFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun apply(item: LoanOptionItem) {
        DialogApply.getInstance(item.loanOptions).show(childFragmentManager, "applyDialog")
    }

    fun send(firstName:String, lastName:String, email:String, phone: String, selectedOption:LoanOptions) {

        if(activity is QuizActivity && quizResult!=null){
            val map = ((quizResult?.result?: HashMap()) as HashMap<String, String>).apply {
                put(QuizKeys.FIRST_NAME, firstName)
                put(QuizKeys.LAST_NAME, lastName)
                put(QuizKeys.EMAIL, email)
                put(QuizKeys.PHONE, phone)
                put(QuizKeys.STEP_DATA_ST_REC_OPTION, quizResult?.recommended?.title?:"")
                put(QuizKeys.STEP_DATA_ST_SEL_OPTION, selectedOption.title)
            }
            (activity as QuizActivity).finishQuiz(map)
        }
    }


    override fun call() {

    }
}