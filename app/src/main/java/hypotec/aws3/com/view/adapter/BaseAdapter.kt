package hypotec.aws3.com.view.adapter

import android.support.v4.util.SparseArrayCompat
import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import tdd.aws3.com.tddexample.view.ViewType
import hypotec.aws3.com.view.adapter.delegate.ViewTypeDelegateAdapter


abstract class BaseAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    protected val items: ArrayList<ViewType> = ArrayList()

    protected var delegateAdapters = SparseArrayCompat<ViewTypeDelegateAdapter<*, *>>()


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return delegateAdapters.get(viewType).onCreateViewHolder(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (delegateAdapters.get(getItemViewType(position)) as ViewTypeDelegateAdapter<ViewType, RecyclerView.ViewHolder>).onBindViewHolder(holder, items[position])
    }

}