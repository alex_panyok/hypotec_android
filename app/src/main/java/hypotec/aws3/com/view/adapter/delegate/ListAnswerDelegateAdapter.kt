package hypotec.aws3.com.view.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.ListAnswerItem
import hypotec.aws3.com.util.inflate
import kotlinx.android.synthetic.main.item_answer_item.view.*


class ListAnswerDelegateAdapter(val onAnswerSelectListener: OnAnswerSelectListener) : ViewTypeDelegateAdapter<ListAnswerItem, ListAnswerDelegateAdapter.ListAnswerItemHolder> {
    override fun onCreateViewHolder(parent: ViewGroup): ListAnswerItemHolder {
        return ListAnswerItemHolder(parent)
    }

    override fun onBindViewHolder(holder: ListAnswerItemHolder, item: ListAnswerItem) {
        holder.bind(item)
    }


    interface OnAnswerSelectListener{
        fun select(item:ListAnswerItem)
    }


    inner class ListAnswerItemHolder(parent:ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_answer_item)) {

        fun bind(item:ListAnswerItem){
            itemView.checkCard.setText(item.title, item.subTitle)
            itemView.checkCard.isActivated = item.isActivated?:false
            itemView.checkCard.setOnClickListener {
                onAnswerSelectListener.select(item)
            }
        }

    }
}