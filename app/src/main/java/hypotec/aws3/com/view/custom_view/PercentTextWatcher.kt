package hypotec.aws3.com.view.custom_view

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText
import java.lang.ref.WeakReference


class PercentTextWatcher(editText: EditText, private val onNumberListener: (Double) -> Unit = {}) : TextWatcher {
    private val editTextWeakReference: WeakReference<EditText> = WeakReference(editText)


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get() ?: return
        val s = editable.toString()
        editText.removeTextChangedListener(this)
        var cleanString =
                if (s.contains("%") || s.isEmpty())
                    s.replace("[%]".toRegex(), "")
                else {
                    s.replaceRange(s.length - 1 until s.length, "")
                }

        if (!cleanString.isEmpty() && cleanString.first() == '0' && cleanString.indexOf(".") != 1 || !cleanString.isEmpty() && cleanString.first() == '.') {
            cleanString = cleanString.replaceFirst(cleanString[0].toString(), "")
        }

        if (!cleanString.isEmpty() && cleanString.indexOf(".") != -1 && cleanString.substring(cleanString.indexOf(".") until cleanString.length).length > 3) {
            cleanString = cleanString.replaceRange(cleanString.length - 1 until cleanString.length, "")
        }

        if (!cleanString.isEmpty() && cleanString.toDouble() > 100 || !cleanString.isEmpty() && cleanString.toDouble() == 100.toDouble() && cleanString.indexOf(".") == cleanString.length - 1)
            cleanString = if(cleanString.indexOf(".")==-1) cleanString.substring(0 until cleanString.length - 1) else cleanString.replaceRange((cleanString.indexOf(".")-1..(cleanString.indexOf(".")-1)),"")


        if (cleanString.isEmpty())
            cleanString = "0"
        editText.setText(cleanString.plus("%"))
        editText.setSelection(editText.text.length)
        onNumberListener.invoke((cleanString.toDouble() * 100))
        editText.addTextChangedListener(this)
    }
}