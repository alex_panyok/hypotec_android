package hypotec.aws3.com.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.util.inflate
import hypotec.aws3.com.util.startActionPhone
import kotlinx.android.synthetic.main.fragment_finish.*


class FinishFragment : Fragment() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_finish)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        call.setOnClickListener{
            activity?.startActionPhone(getString(R.string.raw_phone))
        }
    }


    companion object {
        fun create(): FinishFragment {
            return FinishFragment()
        }
    }
}