package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.ListAnswerItem
import hypotec.aws3.com.domain.ListQuizItem
import hypotec.aws3.com.util.inflate
import hypotec.aws3.com.view.adapter.AdapterQuizList
import hypotec.aws3.com.view.adapter.delegate.ListAnswerDelegateAdapter
import hypotec.aws3.com.viewmodel.QuizListViewModel
import kotlinx.android.synthetic.main.fragment_list_quiz.*

class QuizListFragment : QuizFragment<ListQuizItem, QuizListViewModel>(), ListAnswerDelegateAdapter.OnAnswerSelectListener {


    private val adapterQuiz = AdapterQuizList(this)


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_list_quiz)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.liveData.observe(this, Observer {
            if (it != null)
                adapterQuiz.setItems(it.answers, it.answerItem)
        })
    }


    override fun initViews() {
        recycleView.apply {
            adapter = adapterQuiz
            layoutManager = LinearLayoutManager(activity)
        }
    }

    override fun select(item: ListAnswerItem) {
        viewModel.setAnswer(item)
        next()
    }


    companion object {
        fun create(bundle: Bundle): QuizListFragment {
            val fragment = QuizListFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

}