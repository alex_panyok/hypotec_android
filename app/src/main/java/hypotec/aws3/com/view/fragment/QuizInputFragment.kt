package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.text.InputFilter
import android.text.method.DigitsKeyListener
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.mobsandgeeks.saripaar.ValidationError
import com.mobsandgeeks.saripaar.Validator
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.InputQuizItem
import hypotec.aws3.com.enums.InputType
import hypotec.aws3.com.util.TextsUtils
import hypotec.aws3.com.util.clean
import hypotec.aws3.com.util.inflate
import hypotec.aws3.com.validation.QuickAlwaysApprove
import hypotec.aws3.com.validation.QuickRuleDollar
import hypotec.aws3.com.validation.QuickRuleZipCode
import hypotec.aws3.com.view.custom_view.MoneyTextWatcher
import hypotec.aws3.com.viewmodel.QuizInputModel
import kotlinx.android.synthetic.main.fragment_input_quiz.*


class QuizInputFragment : QuizFragment<InputQuizItem, QuizInputModel>(), Validator.ValidationListener {


    override fun initViews() {
        next.setOnClickListener {
            TextsUtils.clearFocus(activity, et)
            validator.validate()
        }
    }


    private val validator = Validator(this).apply {
        setValidationListener(this@QuizInputFragment)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_input_quiz)

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.liveData.observe(this, Observer {
            if (it != null)
                set(it)
        })
    }


    private fun set(inputQuizItem: InputQuizItem) {
        when (inputQuizItem.inputType) {
            InputType.ZIP_CODE -> {
                et.inputType = android.text.InputType.TYPE_CLASS_NUMBER
                et.filters = arrayOf(InputFilter.LengthFilter(5))
                et.keyListener = DigitsKeyListener.getInstance("0123456789")
                validator.put(et, QuickRuleZipCode())
            }
            InputType.US_DOLLAR -> {
                et.inputType = android.text.InputType.TYPE_CLASS_NUMBER
                et.addTextChangedListener(MoneyTextWatcher(et))
                validator.put(et, QuickRuleDollar())
            }

            InputType.US_DOLLAR_NOT_REQUIRE -> {
                et.inputType = android.text.InputType.TYPE_CLASS_NUMBER
                et.addTextChangedListener(MoneyTextWatcher(et))
                validator.put(et, QuickAlwaysApprove())
            }
        }
        subHint.text = inputQuizItem.subHint ?: ""
        et.hint = inputQuizItem.hint
        et.setText(inputQuizItem.answerItem?.getAnswerValue() ?: "")
    }


    override fun onValidationFailed(errors: MutableList<ValidationError>) {
        for (error in errors) {
            showSnack(error.getCollatedErrorMessage(activity))
        }
    }

    override fun onValidationSucceeded() {
        viewModel.setAnswer(et.text.toString().clean())
        next()
    }


    companion object {
        fun create(bundle: Bundle): QuizInputFragment {
            val fragment = QuizInputFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}