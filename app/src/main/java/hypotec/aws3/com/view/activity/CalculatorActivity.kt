package hypotec.aws3.com.view.activity

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import hypotec.aws3.com.R
import hypotec.aws3.com.util.*
import hypotec.aws3.com.view.custom_view.LongTextWatcher
import hypotec.aws3.com.view.custom_view.MoneyTextWatcher
import hypotec.aws3.com.view.custom_view.PercentTextWatcher
import hypotec.aws3.com.viewmodel.CalculatorModel
import kotlinx.android.synthetic.main.activity_calculator.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class CalculatorActivity : BaseActivity<CalculatorModel>() {


    override fun layoutId(): Int {
        return R.layout.activity_calculator
    }
    private val onNumberListener: (Any) -> Unit = {
        calculate()
    }

    override fun setUpActionBar() {
        super.setUpActionBar()
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_back))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        actionBarTitle.text = getString(R.string.calc_mort)
    }



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.liveDataResult.observe(this, Observer {
            if(it!=null)
                payment.text = formatCurrency(it)
        })

        salePrice.addTextChangedListener(MoneyTextWatcher(salePrice, onNumberListener))
        downPayment.addTextChangedListener(PercentTextWatcher(downPayment, onNumberListener))
        rate.addTextChangedListener(PercentTextWatcher(rate, onNumberListener))
        term.addTextChangedListener(LongTextWatcher(term, onNumberListener))
        calculate.setOnClickListener{
            TextsUtils.clearFocus(this, salePrice, downPayment, rate, term)
            calculate()
        }
    }


    private fun calculate(){
        viewModel.calculate(salePrice.text.toString().cleanCurrency(),
                downPayment.text.toString().cleanPercent(),
                rate.text.toString().cleanPercent(),
                if(term.text.toString().isEmpty()) 0L else term.text.toString().toLong())
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_call -> startActionPhone(getString(R.string.raw_phone))
        }
        return true
    }



    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_calc_menu, menu)
        return true
    }
}