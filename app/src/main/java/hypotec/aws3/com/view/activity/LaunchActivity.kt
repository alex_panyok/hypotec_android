package hypotec.aws3.com.view.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import hypotec.aws3.com.R
import hypotec.aws3.com.util.launchActivity
import kotlinx.android.synthetic.main.activity_launch.*


class LaunchActivity:AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launch)
        next.setOnClickListener {
            launchActivity<MainActivity> {  }
            finish()
        }
    }
}