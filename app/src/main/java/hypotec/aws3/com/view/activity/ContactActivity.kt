package hypotec.aws3.com.view.activity

import android.content.ActivityNotFoundException
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import hypotec.aws3.com.R
import hypotec.aws3.com.databinding.ActivityContactBinding
import hypotec.aws3.com.util.*
import kotlinx.android.synthetic.main.activity_contact.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class ContactActivity : AppCompatActivity() {

    private val addresses: Map<Int, String> by lazy {
        mapOf(
                R.id.place1 to getString(R.string.place1_address),
                R.id.place2 to getString(R.string.place2_address),
                R.id.place3 to getString(R.string.place3_address))
    }


    private val socials: Map<Int, String> by lazy {
        mapOf(
                R.id.facebook to getString(R.string.facebook_app_url),
                R.id.twitter to getString(R.string.twitter_app_url),
                R.id.linkedin to getString(R.string.linkedin_app_url),
                R.id.youtube to getString(R.string.youtube_app_url),
                R.id.googlePlus to getString(R.string.gplus_app_url),
                R.id.yelp to getString(R.string.yelp_app_url))
    }


    private val socialsWeb: Map<Int, String> by lazy {
        mapOf(
                R.id.facebook to getString(R.string.facebook_url),
                R.id.twitter to getString(R.string.twitter_url),
                R.id.linkedin to getString(R.string.linkedin_url),
                R.id.youtube to getString(R.string.youtube_url),
                R.id.googlePlus to getString(R.string.gplus_url),
                R.id.yelp to getString(R.string.yelp_url))
    }


    private val onPlaceClickListener: (View) -> Unit = {
        startActionGeo(addresses[it.id] ?: "")
    }


    private val onSocialClickListener: (View) -> Unit = {
        try {
            startActionView(socials[it.id] ?: "")
        } catch (e: ActivityNotFoundException) {
            startActionView(socialsWeb[it.id] ?: "")
        }


    }


    private val onContactClickListener: (View) -> Unit = {
        when (it.id) {
            R.id.phone -> {
                startActionPhone(getString(R.string.raw_phone))
            }
            R.id.email -> {
                startActionEmail(getString(R.string.raw_email))
            }
        }

    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityContactBinding = DataBindingUtil.setContentView(this, R.layout.activity_contact)
        binding.executePendingBindings()
        setUpActionBar()
        setUpViews()
    }


    private fun setUpActionBar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_back))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        actionBarTitle.text = getString(R.string.contacts)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return true
    }


    private fun setUpViews() {
        phone.setTextHtml(getString(R.string.contacts_phone))
        email.setTextHtml(getString(R.string.contacts_email))

        phone.setOnClickListener(onContactClickListener)
        email.setOnClickListener(onContactClickListener)

        place1.setOnClickListener(onPlaceClickListener)
        place2.setOnClickListener(onPlaceClickListener)
        place3.setOnClickListener(onPlaceClickListener)

        facebook.setOnClickListener(onSocialClickListener)
        linkedin.setOnClickListener(onSocialClickListener)
        twitter.setOnClickListener(onSocialClickListener)
        youtube.setOnClickListener(onSocialClickListener)
        googlePlus.setOnClickListener(onSocialClickListener)
        yelp.setOnClickListener(onSocialClickListener)
    }
}