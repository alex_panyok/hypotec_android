package hypotec.aws3.com.view.adapter


import hypotec.aws3.com.domain.ItemCall
import hypotec.aws3.com.domain.LoanOptionItem
import hypotec.aws3.com.util.AdapterKeys
import hypotec.aws3.com.view.adapter.delegate.ItemCallDelegateAdapter
import hypotec.aws3.com.view.adapter.delegate.LoanOptionDelegateAdapter


class AdapterLoanOption(private val onAdapterLoanOptionListener: OnAdapterLoanOptionListener) : BaseAdapter(), LoanOptionDelegateAdapter.OnApplyListener {
    override fun apply(item: LoanOptionItem) {
        onAdapterLoanOptionListener.apply(item)
    }


    init {
        delegateAdapters.put(AdapterKeys.LOAN_OPTION, LoanOptionDelegateAdapter(this))
        delegateAdapters.put(AdapterKeys.CALL_ITEM, ItemCallDelegateAdapter())
    }


    override fun getItemViewType(position: Int): Int {
        return items[position].getViewType()
    }

    interface OnAdapterLoanOptionListener{
        fun apply(item: LoanOptionItem)
        fun call()
    }


    fun setItems(list:List<LoanOptionItem>){
        items.clear()
        items.addAll(list)
        items.add(ItemCall())
        notifyDataSetChanged()
    }
}