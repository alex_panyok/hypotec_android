package hypotec.aws3.com.view.custom_view

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.util.Log
import android.widget.EditText
import hypotec.aws3.com.util.cleanCurrency
import hypotec.aws3.com.util.formatCurrency

import java.lang.ref.WeakReference


class MoneyTextWatcher(editText: EditText, val onNumberListener: (Long) -> Unit = {}) : TextWatcher {
    private val editTextWeakReference: WeakReference<EditText> = WeakReference(editText.apply {
        filters =  arrayOf(InputFilter.LengthFilter(24))
    })


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(cs: CharSequence, start: Int, before: Int, count: Int) {


    }

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get() ?: return
        val s = editable.toString()
        editText.removeTextChangedListener(this)
        val number = s.cleanCurrency()
        val formatted = formatCurrency(number)
//        editable.replace(0, editable.length, formatted, 0, formatted.length)
        editText.setText(formatted)
        editText.setSelection(formatted.length)
        onNumberListener.invoke(number)
        editText.addTextChangedListener(this)
    }


}