package hypotec.aws3.com.view.adapter

import hypotec.aws3.com.domain.AnswerItem
import hypotec.aws3.com.domain.ListAnswerItem
import hypotec.aws3.com.util.AdapterKeys
import hypotec.aws3.com.view.adapter.delegate.ListAnswerDelegateAdapter
import tdd.aws3.com.tddexample.view.ViewType


class AdapterQuizList(onAnswerSelectListener: ListAnswerDelegateAdapter.OnAnswerSelectListener) : BaseAdapter() {




    init {
        delegateAdapters.put(AdapterKeys.LIST_ANSWER, ListAnswerDelegateAdapter(onAnswerSelectListener))

    }


    override fun getItemViewType(position: Int): Int {
        return items[position].getViewType()
    }


    fun setItems(list:List<AnswerItem>, answerItem: AnswerItem?){
        items.clear()
        items.addAll(list.map { it as ListAnswerItem }.map { it.isActivated = (it == answerItem); it })
        notifyDataSetChanged()
    }



}