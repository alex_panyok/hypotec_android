package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.CalcQuizItem
import hypotec.aws3.com.enums.CalcEnum
import hypotec.aws3.com.util.*
import hypotec.aws3.com.view.custom_view.MoneyTextWatcher
import hypotec.aws3.com.view.custom_view.PercentTextWatcher
import hypotec.aws3.com.viewmodel.QuizCalcModel
import kotlinx.android.synthetic.main.fragment_calc_quiz.*


class QuizCalcFragment : QuizFragment<CalcQuizItem, QuizCalcModel>() {

    private lateinit var percentWatcher: PercentTextWatcher


    private lateinit var downPaymentWatcher: MoneyTextWatcher

    private lateinit var salePaymentWatcher: MoneyTextWatcher

    override fun initViews() {
        next.setOnClickListener {
            TextsUtils.clearFocus(activity, salePrice, downPayment, percent)
            viewModel.setAnswerItem(salePrice.text.toString().clean(), percent.text.toString().clean(), downPayment.text.toString().clean())
            next()
        }
        downPaymentWatcher = MoneyTextWatcher(downPayment) {
            calc(CalcEnum.DOWN)
        }
        percentWatcher = PercentTextWatcher(percent) {
            calc(CalcEnum.PERCENT)
        }
        salePaymentWatcher = MoneyTextWatcher(salePrice) {
            downPayment.isEnabled = it>0
            percent.isEnabled = it>0
            calc(CalcEnum.SALE)
        }
        salePrice.addTextChangedListener(salePaymentWatcher)
        downPayment.addTextChangedListener(downPaymentWatcher)
        percent.addTextChangedListener(percentWatcher)
    }

    private fun calc(calcEnum: CalcEnum) {
        viewModel.calc(salePrice.text.toString().cleanCurrency(), downPayment.text.toString().cleanCurrency(), percent.text.toString().cleanPercent(), calcEnum)
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_calc_quiz)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        viewModel.liveDataPercent.observe(this, Observer {
            if(!percent.isFocused) {
                if (it.toString().cleanPercent() != percent.text.toString().cleanPercent()) {
                    percent.removeTextChangedListener(percentWatcher)
                    percent.setText(formatPercent(if (it.toString().cleanPercent() > 1) 1.toDouble() else it.toString().cleanPercent()))
                    percent.setSelection(percent.text.length)
                    percent.addTextChangedListener(percentWatcher)
                }
            }
        })

        viewModel.liveDataDown.observe(this, Observer {
            if(!downPayment.isFocused) {
                downPayment.removeTextChangedListener(downPaymentWatcher)
                downPayment.setText(formatCurrency(it.toString().cleanCurrency()))
                downPayment.setSelection(downPayment.text.length)
                downPayment.addTextChangedListener(downPaymentWatcher)
            }
        })


    }


    companion object {
        fun create(bundle: Bundle): QuizCalcFragment {
            val fragment = QuizCalcFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}