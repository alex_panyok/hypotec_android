package hypotec.aws3.com.view.custom_view

import android.content.Context
import android.support.v7.widget.CardView
import android.util.AttributeSet
import hypotec.aws3.com.util.PixelUtil


class CardLayout (context: Context, attrs: AttributeSet?, defStyleAttr: Int) : CardView(context, attrs, defStyleAttr) {

    constructor(context: Context) : this(context, null, 0)

    constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)


    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val height = (PixelUtil.getScreenHeight(context)/3.toFloat()).toInt()
        super.onMeasure(widthMeasureSpec, MeasureSpec.makeMeasureSpec( height, MeasureSpec.EXACTLY))
    }

}