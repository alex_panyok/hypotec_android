package hypotec.aws3.com.view.custom_view

import android.text.Editable
import android.text.InputFilter
import android.text.TextWatcher
import android.widget.EditText
import hypotec.aws3.com.util.formatCurrency
import java.lang.ref.WeakReference

class LongTextWatcher(editText: EditText, val onNumberListener: (Any) -> Unit = {}) : TextWatcher {
    private val editTextWeakReference: WeakReference<EditText> = WeakReference(editText.apply {
        filters =  arrayOf(InputFilter.LengthFilter(4))
    })


    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

    override fun afterTextChanged(editable: Editable) {
        val editText = editTextWeakReference.get() ?: return
        val s = editable.toString()
        editText.removeTextChangedListener(this)
        onNumberListener.invoke(s)
        editText.addTextChangedListener(this)
    }


}