package hypotec.aws3.com.view.fragment

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import dagger.android.support.AndroidSupportInjection
import hypotec.aws3.com.view.activity.QuizActivity
import kotlinx.android.synthetic.main.layout_fragment_navigation.*


abstract class BaseFragment : Fragment() {


    override fun onAttach(context: Context?) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }


    abstract fun initViews()

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        initViews()
    }





}