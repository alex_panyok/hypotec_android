package hypotec.aws3.com.view.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.LoanOptionItem
import hypotec.aws3.com.util.getTitle
import hypotec.aws3.com.util.inflate
import kotlinx.android.synthetic.main.item_loan_option.view.*

class LoanOptionDelegateAdapter(val onApplyListener: OnApplyListener) : ViewTypeDelegateAdapter<LoanOptionItem, LoanOptionDelegateAdapter.LoanOptionItemHolder> {
    override fun onCreateViewHolder(parent: ViewGroup): LoanOptionItemHolder {
        return LoanOptionItemHolder(parent)
    }

    override fun onBindViewHolder(holder: LoanOptionItemHolder, item: LoanOptionItem) {
        holder.bind(item)
    }

    interface OnApplyListener{
        fun apply(item:LoanOptionItem)
    }




    inner class LoanOptionItemHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_loan_option)) {

        fun bind(item: LoanOptionItem){
            itemView.option.text = if(item.recommended)itemView.context.getString(R.string.recommended) else String.format(itemView.context.getString(R.string.option), item.index)
            itemView.title.text = item.loanOptions.getTitle(itemView.context)
            itemView.apply.setOnClickListener{
                onApplyListener.apply(item)
            }
        }

    }
}