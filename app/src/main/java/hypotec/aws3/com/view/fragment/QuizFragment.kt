package hypotec.aws3.com.view.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import android.view.View
import hypotec.aws3.com.domain.AnswerItem
import hypotec.aws3.com.domain.QuizItem
import hypotec.aws3.com.util.Keys
import hypotec.aws3.com.view.activity.QuizActivity
import hypotec.aws3.com.viewmodel.BaseViewModel
import kotlinx.android.synthetic.main.layout_fragment_navigation.*
import kotlinx.android.synthetic.main.layout_fragment_title.*
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


abstract class QuizFragment<Q:QuizItem, VM : BaseViewModel<Q>> : BaseVMFragment<VM>() {



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        titleFragment?.text = arguments?.getParcelable<Q>(Keys.ITEM)?.title ?: ""

        viewModel.liveData.observe(this, Observer {
                addAnswer(it?.answerItem)
        })
        val q:Q? = arguments?.getParcelable(Keys.ITEM)
        if(savedInstanceState==null)
            viewModel.setData(arguments?.getParcelable(Keys.ITEM))

    }


    fun showSnack(message:String){
        if (activity != null && activity is QuizActivity)
            (activity as QuizActivity).showSnack(message)
    }





    fun addAnswer(answerItem: AnswerItem?){
        if (activity != null && activity is QuizActivity && answerItem!=null)
            (activity as QuizActivity).addAnswer(answerItem)
    }

    fun next() {
        if (activity != null && activity is QuizActivity)
            (activity as QuizActivity).next()
    }
}