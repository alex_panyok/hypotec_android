package hypotec.aws3.com.view.activity

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.api.error.RetrofitException
import kotlinx.android.synthetic.main.layout_fragment_navigation.*
import java.lang.reflect.ParameterizedType
import javax.inject.Inject


abstract class BaseActivity<VM:ViewModel>:AppCompatActivity(), HasSupportFragmentInjector {

    lateinit var viewModel: VM

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var fragmentInjector: DispatchingAndroidInjector<Fragment>


    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(layoutId())
        setUpActionBar()
        createViewModel()

    }


    protected fun chooseErrorMessage(error: Throwable): String {
        return if(error is RetrofitException) {
            when (error.kind) {
                RetrofitException.Kind.HTTP -> {
                    getString(R.string.error_server)
                }
                RetrofitException.Kind.NETWORK -> {
                    getString(R.string.error_network)
                }
                RetrofitException.Kind.UNEXPECTED -> {
                    getString(R.string.error_unexpected)
                }
            }
        }else
            getString(R.string.error_unexpected)
    }


    private fun createViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).
                get((javaClass.genericSuperclass as ParameterizedType).actualTypeArguments[0] as Class<VM>)
    }


    override fun supportFragmentInjector(): AndroidInjector<Fragment> {
        return fragmentInjector
    }

    open protected fun setUpActionBar(){}


    abstract fun layoutId():Int
}