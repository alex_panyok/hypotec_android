package hypotec.aws3.com.view.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup

interface ViewTypeDelegateAdapter<in T:Any , VH : RecyclerView.ViewHolder> {
    fun onCreateViewHolder(parent: ViewGroup): VH

    fun onBindViewHolder(holder: VH, item: T)
}