package hypotec.aws3.com.view.adapter.delegate

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.ItemCall
import hypotec.aws3.com.util.inflate
import hypotec.aws3.com.util.startActionPhone
import kotlinx.android.synthetic.main.item_call.view.*

class ItemCallDelegateAdapter : ViewTypeDelegateAdapter<ItemCall, ItemCallDelegateAdapter.ItemCallItemHolder> {

    override fun onCreateViewHolder(parent: ViewGroup): ItemCallItemHolder {
        return ItemCallItemHolder(parent)
    }

    override fun onBindViewHolder(holder: ItemCallItemHolder, item: ItemCall) {
        holder.bind(item)
    }




    inner class ItemCallItemHolder(parent: ViewGroup) : RecyclerView.ViewHolder(parent.inflate(R.layout.item_call)) {

        fun bind(item: ItemCall){
            itemView.root.setOnClickListener {
                it.context.startActionPhone(it.context.getString(R.string.raw_phone))
            }
        }

    }
}