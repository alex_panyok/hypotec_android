package hypotec.aws3.com.view.activity

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.view.Menu
import android.view.MenuItem
import android.view.View
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.*
import hypotec.aws3.com.enums.Quiz
import hypotec.aws3.com.util.Keys
import hypotec.aws3.com.util.launchActivity
import hypotec.aws3.com.util.snack
import hypotec.aws3.com.util.startActionPhone
import hypotec.aws3.com.view.fragment.*
import hypotec.aws3.com.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_quiz.*
import kotlinx.android.synthetic.main.layout_fragment_navigation.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class QuizActivity : BaseActivity<MainViewModel>() {


    override fun layoutId(): Int {
        return R.layout.activity_quiz
    }


    override fun setUpActionBar() {
        super.setUpActionBar()
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeAsUpIndicator(ContextCompat.getDrawable(this, R.drawable.ic_back))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowTitleEnabled(false)
        chooseTitle()
    }


    private fun chooseTitle() {
        when (intent.getSerializableExtra(Keys.QUIZ)) {
            Quiz.BUY_HOME -> {
                actionBarTitle.text = getString(R.string.buy_home)
            }
            Quiz.REFINANCE -> {
                actionBarTitle.text = getString(R.string.refinance)
            }
        }

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_quiz_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> onBackPressed()
            R.id.action_calc -> launchActivity<CalculatorActivity> { }
            R.id.action_call -> startActionPhone(getString(R.string.raw_phone))
        }
        return true
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        skip?.setOnClickListener {
            next()
        }
        prev?.setOnClickListener {
            prev()
        }
        viewModel.liveDataQuiz.observe(this, Observer {
            if (it != null) {
                progressSteps.max = it.size

                progressSteps.secondaryProgress = it.size
            }
        })
        viewModel.liveDataCurrentQuiz.observe(this, Observer {
            if (it?.quizItem != null) {
                progress.visibility = View.GONE
                setFragment(chooseFragment(it.quizItem), it.quizItem.javaClass.simpleName + getIndex(), getIndex() > 0, it.prev)
            }
        })
        viewModel.liveDataIndex.observe(this, Observer {
            if (it != null)
                progressSteps.progress = it + 1
        })
        viewModel.liveDataError.observe(this, Observer {
            if (it != null) {
                progress.visibility = View.GONE
                showSnack(chooseErrorMessage(it))
                viewModel.liveDataError.value = null
            }
        })
        if (savedInstanceState == null || savedInstanceState.isEmpty)
            viewModel.getQuiz(intent.getSerializableExtra(Keys.QUIZ) as Quiz)

    }


    override fun onSaveInstanceState(outState: Bundle?) {
        if (isChangingConfigurations)
            super.onSaveInstanceState(outState)
    }

    private fun chooseFragment(item: Any): Fragment {
        skip?.visibility = if (item is QuizItem && item.canSkip) View.VISIBLE else View.INVISIBLE
        prev?.visibility = if (item is QuizItem && getIndex() > 0) View.VISIBLE else View.INVISIBLE
        when (item) {
            is QuizItem -> {
                val args = Bundle().apply { putParcelable(Keys.ITEM, item) }
                when (item) {
                    is ListQuizItem -> return QuizListFragment.create(args)
                    is InputQuizItem -> return QuizInputFragment.create(args)
                    is CalcQuizItem -> return QuizCalcFragment.create(args)
                    is SimpleQuizItem -> return QuizSimpleFragment.create(args)
                }
            }
            is QuizResult -> {
                progressSteps.visibility = View.GONE
                return QuizResultFragment.create(Bundle().apply { putParcelable(Keys.ITEM, item) })
            }
            is FinishItem -> return FinishFragment.create()
        }
        return QuizListFragment()
    }


    fun setFragment(fragment: Fragment, tag: String, backStack: Boolean = true, prev: Boolean) {
        if (!prev) {
            if (supportFragmentManager.findFragmentByTag(tag) == null) {
                val transaction = supportFragmentManager.beginTransaction()
                if (!supportFragmentManager.fragments.none { it is QuizFragment<*, *> || it is QuizResultFragment || it is FinishFragment })
                    transaction.setCustomAnimations(
                            android.R.anim.fade_in,
                            android.R.anim.fade_out,
                            android.R.anim.fade_in,
                            android.R.anim.fade_out)
                transaction.replace(R.id.container, fragment, tag)
                if (backStack)
                    transaction.addToBackStack(tag)
                transaction.commit()
            }
        } else
            supportFragmentManager.popBackStack()
    }

    private fun getIndex(): Int {
        return viewModel.index
    }

    fun next() {
        viewModel.next()
    }

    fun showSnack(message: String) {
        main.snack(message)
    }


    fun finishQuiz(map: HashMap<String, String>) {
        progress.visibility = View.VISIBLE
        viewModel.finish(map)
    }

    fun addAnswer(answerItem: AnswerItem) {
        viewModel.addAnswer(answerItem)
    }

    private fun prev() {
        viewModel.prev()
    }


    override fun onBackPressed() {
        finish()
    }


}
