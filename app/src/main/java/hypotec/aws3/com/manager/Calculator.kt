package hypotec.aws3.com.manager

import javax.inject.Inject


class Calculator @Inject constructor() {

    fun calculate(salePrice:Long, downPayment:Double, rate:Double, term:Long):Long{
        if(term==0L)
            return 0L
        val n = term.toDouble() * 12
        val c = rate / 12
        val dp = 1 - downPayment
        val l = salePrice.toDouble() * dp
        return  Math.abs(Math.round((l*(c*Math.pow(1+c,n)))/(Math.pow(1+c,n)-1)))
    }


}