package hypotec.aws3.com.validation

import android.content.Context
import android.support.design.widget.TextInputEditText
import com.mobsandgeeks.saripaar.QuickRule
import hypotec.aws3.com.R
import hypotec.aws3.com.util.cleanCurrency

class QuickRuleDollar : QuickRule<TextInputEditText>() {

    override fun isValid(view: TextInputEditText): Boolean {
        return view.text.toString().cleanCurrency()>0
    }

    override fun getMessage(context: Context): String {
        return context.getString(R.string.greater_than_zero)
    }
}