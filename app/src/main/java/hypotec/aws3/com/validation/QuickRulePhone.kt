package hypotec.aws3.com.validation

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.util.Patterns
import com.mobsandgeeks.saripaar.QuickRule
import hypotec.aws3.com.R

class QuickRulePhone : QuickRule<TextInputEditText>() {

    override fun isValid(view: TextInputEditText): Boolean {
        return Patterns.PHONE.matcher(view.text).matches()
    }

    override fun getMessage(context: Context): String {
        return context.getString(R.string.invalid_phone)
    }
}