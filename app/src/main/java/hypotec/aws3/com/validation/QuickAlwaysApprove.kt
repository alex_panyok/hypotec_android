package hypotec.aws3.com.validation

import android.content.Context
import android.support.design.widget.TextInputEditText
import com.mobsandgeeks.saripaar.QuickRule
import hypotec.aws3.com.R

class QuickAlwaysApprove : QuickRule<TextInputEditText>() {

    override fun isValid(view: TextInputEditText): Boolean {
        return true
    }

    override fun getMessage(context: Context): String {
        return ""
    }
}