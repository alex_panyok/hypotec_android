package hypotec.aws3.com.validation

import android.content.Context
import android.support.design.widget.TextInputEditText
import com.mobsandgeeks.saripaar.QuickRule
import hypotec.aws3.com.R

class QuickRuleZipCode : QuickRule<TextInputEditText>() {

    override fun isValid(view: TextInputEditText): Boolean {
        return !view.text.isEmpty() && view.text.toString().length>4
    }

    override fun getMessage(context: Context): String {
        return context.getString(R.string.invalid_zip_code)
    }
}