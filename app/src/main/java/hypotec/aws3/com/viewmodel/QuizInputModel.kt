package hypotec.aws3.com.viewmodel

import hypotec.aws3.com.domain.InputAnswerItem
import hypotec.aws3.com.domain.InputQuizItem
import hypotec.aws3.com.enums.LoanOptions
import javax.inject.Inject

class QuizInputModel @Inject constructor() : BaseViewModel<InputQuizItem>() {


    fun setAnswer(text: String) {
        liveData.value?.answerItem = InputAnswerItem(1, options = listOf(LoanOptions.VA, LoanOptions.FHA, LoanOptions.CONV), title = text)
        liveData.postValue(liveData.value)
    }

}