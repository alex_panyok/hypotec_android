package hypotec.aws3.com.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import hypotec.aws3.com.manager.Calculator
import javax.inject.Inject


class CalculatorModel @Inject constructor(val calculator: Calculator):ViewModel() {

    val liveDataResult:MutableLiveData<Long> = MutableLiveData()

    fun calculate(salePrice:Long, downPayment:Double, rate:Double, term:Long){
        liveDataResult.postValue(calculator.calculate(salePrice, downPayment, rate, term))
    }
}