package hypotec.aws3.com.viewmodel

import hypotec.aws3.com.domain.SimpleQuizItem
import javax.inject.Inject


class QuizSimpleModel @Inject constructor() : BaseViewModel<SimpleQuizItem>() {

    fun setAnswer(positive: Boolean){
        liveData.value?.answerItem = if(positive) liveData.value?.answers?.first else liveData.value?.answers?.second
        liveData.postValue(liveData.value)
    }

}