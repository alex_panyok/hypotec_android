package hypotec.aws3.com.viewmodel

import hypotec.aws3.com.domain.ListAnswerItem
import hypotec.aws3.com.domain.ListQuizItem
import javax.inject.Inject


class QuizListViewModel @Inject constructor():BaseViewModel<ListQuizItem>() {


    fun setAnswer(answerItem: ListAnswerItem){
        liveData.value?.answerItem = answerItem
        liveData.postValue(liveData.value)
    }




}