package hypotec.aws3.com.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import hypotec.aws3.com.domain.CalcQuizItem
import hypotec.aws3.com.domain.ListQuizItem
import hypotec.aws3.com.domain.QuizItem


abstract class BaseViewModel<Q : QuizItem>:ViewModel(){
    val liveData: MutableLiveData<Q> = MutableLiveData()

    fun setData(quizItem: Q?){
        Log.i("QUIZzzzz", quizItem.toString())
        if(liveData.value==null && quizItem!=null)
            liveData.postValue(quizItem)
    }
}