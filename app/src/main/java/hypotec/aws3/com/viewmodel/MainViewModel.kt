package hypotec.aws3.com.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import hypotec.aws3.com.domain.*
import hypotec.aws3.com.domain.repo.QuizRepo
import hypotec.aws3.com.enums.LoanOptions
import hypotec.aws3.com.enums.Quiz
import hypotec.aws3.com.util.QuizKeys
import javax.inject.Inject


class MainViewModel @Inject constructor(private val quizRepo: QuizRepo) : ViewModel() {

    var index: Int = -1


    private val answersList: MutableList<AnswerItem> = ArrayList()


    val liveDataIndex = MutableLiveData<Int>()

    val liveDataQuiz = MutableLiveData<List<QuizItem>>()

    val liveDataCurrentQuiz = MutableLiveData<QuizItemData>()

    val liveDataError = MutableLiveData<Throwable>()

    var quiz: Quiz = Quiz.BUY_HOME


    fun getQuiz(quiz: Quiz) {
        this.quiz = quiz
        quizRepo.getQuiz(quiz).subscribe {
            liveDataQuiz.value = it
            liveDataQuiz.postValue(it)
            index = if (index < 0) 0 else index
            liveDataIndex.postValue(index)
            liveDataCurrentQuiz.postValue(QuizItemData(liveDataQuiz.value?.get(index)))
        }
    }


    fun next() {
        if (index < (liveDataQuiz.value?.size?.minus(1)) ?: 0) {
            index++
            if (checkSkipItem(index)) {
                liveDataQuiz.value?.get(index)?.answerItem = null
                index++
            }
            liveDataIndex.postValue(index)
            liveDataCurrentQuiz.postValue(QuizItemData(liveDataQuiz.value?.get(index)))
        } else {
            liveDataCurrentQuiz.postValue(QuizItemData(calcResult()))
        }
    }

    fun finish(map: HashMap<String, String>) {
        quizRepo.send(map.apply {
            put(QuizKeys.ACTION, when (quiz) {
                Quiz.BUY_HOME -> {
                    "aws_theme_sing_up_option"
                }
                Quiz.REFINANCE -> {
                    "aws_theme_sing_up_refinance_option"
                }
            })
        }).subscribe({ liveDataCurrentQuiz.postValue(QuizItemData(FinishItem())) },
                { liveDataError.postValue(it) })

    }

    fun prev() {
        if (index > 0) {
            index--
            if (checkSkipItem(index))
                index--
            liveDataIndex.postValue(index)
            liveDataCurrentQuiz.postValue(QuizItemData(liveDataQuiz.value?.get(index), true))
        }
    }


    private fun checkSkipItem(index: Int): Boolean {
        liveDataQuiz.value?.forEach {
            val answer = it.answerItem
            if (answer != null && answer is SimpleAnswerItem) {
                if (answer.skipStep == index)
                    return true
            }
        }
        return false
    }

    private fun calcResult(): QuizResult {
        val scoreOptions: HashMap<LoanOptions, Int> = HashMap()
        val map = HashMap<String, String>()
        liveDataQuiz.value?.forEach { quiz ->
            val answer = quiz.answerItem
            if (answer != null) {
                if (quiz !is CalcQuizItem)
                    map.put(quiz.key, answer.getAnswerValue())
                else {
                    (answer as CalcAnswerItem).answers.forEachIndexed { index, s ->
                        map.put(quiz.keys[index], s)
                    }
                }

                answer.list.forEach {
                    val score = scoreOptions[it]
                    if (score != null)
                        scoreOptions[it] = score + answer.score
                    else
                        scoreOptions[it] = answer.score
                }
                if (answer.reset)
                    return QuizResult(emptyMap(), LoanOptions.ANY)
            }
        }
        return QuizResult(map, scoreOptions.maxBy { it.value }?.key ?: LoanOptions.ANY)
    }


    fun addAnswer(answerItem: AnswerItem) {
        if (index < answersList.size)
            answersList[index] = answerItem
        else
            answersList.add(answerItem)
    }


}