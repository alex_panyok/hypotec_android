package hypotec.aws3.com.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import hypotec.aws3.com.domain.LoanOptionItem
import hypotec.aws3.com.domain.QuizResult
import hypotec.aws3.com.enums.LoanOptions
import javax.inject.Inject


class QuizResultModel @Inject constructor():ViewModel()  {

    val liveData:MutableLiveData<List<LoanOptionItem>> = MutableLiveData()


    fun setResult(result:QuizResult?){
        if(result!=null) {
            val list = ArrayList<LoanOptionItem>()
            if(result.recommended==LoanOptions.ANY) {
                liveData.postValue(emptyList())
                return
            }

            list.add(LoanOptionItem(LoanOptions.FHA, recommended = result.recommended == LoanOptions.FHA))
            list.add(LoanOptionItem(LoanOptions.VA,  recommended = result.recommended == LoanOptions.VA))
            list.add(LoanOptionItem(LoanOptions.CONV,  recommended = result.recommended == LoanOptions.CONV))
            liveData.postValue(list.sortedByDescending { it.recommended }.mapIndexed { i, loanOptionItem -> loanOptionItem.apply { index = i+1 } })
        }
    }



}