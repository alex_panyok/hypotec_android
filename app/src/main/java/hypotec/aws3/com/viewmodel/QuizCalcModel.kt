package hypotec.aws3.com.viewmodel

import android.arch.lifecycle.MutableLiveData
import hypotec.aws3.com.domain.CalcAnswerItem
import hypotec.aws3.com.domain.CalcQuizItem
import hypotec.aws3.com.enums.CalcEnum
import hypotec.aws3.com.enums.LoanOptions
import java.util.*
import javax.inject.Inject

class QuizCalcModel @Inject constructor() : BaseViewModel<CalcQuizItem>() {


    val liveDataPercent: MutableLiveData<Double> = MutableLiveData()

    val liveDataDown: MutableLiveData<Long> = MutableLiveData()

    val liveDataSale: MutableLiveData<Long> = MutableLiveData()

    fun calc(sale: Long, down: Long, percent: Double, calcEnum: CalcEnum) {
        val downC: Long
        val percentC: Double
        val currentValueSale = liveDataSale.value ?: 0L
        val currentValueDown = liveDataDown.value ?: 0L
        val currentValuePercent = liveDataPercent.value ?: 0L
        if (currentValueSale == sale && currentValueDown == down && currentValuePercent == (percent * 100).toLong())
            return
        when (calcEnum) {
            CalcEnum.SALE -> {
                if (down > 0) {
                    downC = down
                    percentC = if (sale == 0L) 0.toDouble() else (down.toDouble() / sale.toDouble()) * 100
                } else {
                    percentC = (percent * 100)
                    downC = (sale * percent).toLong()
                }
            }
            CalcEnum.DOWN -> {
                downC = down
                percentC = if (sale == 0L) 0.toDouble() else (downC.toDouble() / sale.toDouble()) * 100
            }

            CalcEnum.PERCENT -> {
                percentC = (percent * 100)
                downC = (sale * percent).toLong()
            }
        }
        liveDataDown.postValue(downC)
        liveDataPercent.postValue(percentC)
        liveDataSale.postValue(sale)
    }


    fun setAnswerItem(fullPrice: String, downPayment: String, downPaymentPrice: String) {
        val percent = liveDataPercent.value ?: 0.toDouble()
        var option: LoanOptions = LoanOptions.CONV
        if (percent >= 0 && percent <= (3.5)) {
            option = LoanOptions.VA
        } else if (percent > (3.5) && percent <= 5) {
            option = LoanOptions.FHA
        } else if (percent > 5) {
            option = LoanOptions.CONV
        }
        val answerItem = CalcAnswerItem(1, list = listOf(option), answers = listOf(fullPrice, downPayment, downPaymentPrice))
        liveData.value?.answerItem = answerItem
        liveData.postValue(liveData.value)
    }


}