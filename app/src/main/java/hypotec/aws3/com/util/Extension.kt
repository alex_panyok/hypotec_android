package hypotec.aws3.com.util

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ClipData.newIntent
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import hypotec.aws3.com.R
import hypotec.aws3.com.enums.LoanOptions


fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)
}


fun TextView.setTextHtml(text:String){
    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
        this.text = Html.fromHtml(text, Html.FROM_HTML_MODE_COMPACT)
    }else{
        this.text = Html.fromHtml(text)
    }
}


fun Context.startActionPhone(phone:String){
    this.startActivity(Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null)))
}


fun Context.startActionView(url:String){
    if(!url.isEmpty())
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse(url)))
}


fun Context.startActionEmail(email:String){
    if(!email.isEmpty()) {
        val intent = Intent(Intent.ACTION_SEND).apply {
            type = "text/plain"

            putExtra(Intent.EXTRA_EMAIL, email)
        }
        this.startActivity(Intent.createChooser(intent, this.getString(R.string.email)))
    }
}



fun Context.startActionGeo(address:String){
    if(!address.isEmpty())
        this.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=$address")))
}

fun View.snack(message: String?, length: Int = Snackbar.LENGTH_LONG) {
    if (message != null) {
        val snack = Snackbar.make(this, message, length)
        snack.show()
    }
}


fun LoanOptions.getTitle(context:Context):String{
    return when(this){
        LoanOptions.FHA -> context.getString(R.string.fha)
        LoanOptions.VA -> context.getString(R.string.va)
        LoanOptions.CONV -> context.getString(R.string.conv)
        LoanOptions.ANY -> ""
    }
}
inline fun <reified T : Any> newIntent(context: Context): Intent =
        Intent(context, T::class.java)

@SuppressLint("ObsoleteSdkInt")
inline fun <reified T : Any> Activity.launchActivity(
        requestCode: Int = -1,
        options: Bundle? = null,
        noinline init: Intent.() -> Unit = {}) {

    val intent = newIntent<T>(this)
    intent.init()
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
        startActivityForResult(intent, requestCode, options)
    } else {
        startActivityForResult(intent, requestCode)
    }
}