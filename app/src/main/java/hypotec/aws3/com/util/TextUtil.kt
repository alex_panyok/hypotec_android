package hypotec.aws3.com.util

import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.*


fun String.cleanCurrency(): Long {
    val clean = this.replace("[$,.\\u00A0]".toRegex(), "")
    return if(!clean.isEmpty()) clean.toLong() else 0L
}


fun String.cleanPercent(): Double {
    val clean = this.replace("[%,]".toRegex(), "")
    return if(!clean.isEmpty()) clean.toDouble()/100 else 0.toDouble()
}


fun String.clean(): String {
    return this.replace("[%$]".toRegex(), "")
}


fun formatCurrency(number:Long):String{
   val formatter = NumberFormat.getCurrencyInstance(Locale.US).apply {
        currency = Currency.getInstance(Locale.US)
        maximumFractionDigits=0
        minimumFractionDigits=0
    }!!
    return formatter.format(number)
}


fun formatPercent(number:Double):String{
    val formatter = (DecimalFormat.getPercentInstance(Locale.US) as DecimalFormat).apply {

        maximumFractionDigits=2
        minimumFractionDigits=0
    }
    return formatter.format(number)
}