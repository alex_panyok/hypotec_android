package hypotec.aws3.com.util

import android.content.Context
import android.support.design.widget.TextInputEditText
import android.support.design.widget.TextInputLayout
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText


object TextsUtils {

    fun clearFocus(context: Context?, vararg editTexts: EditText) {
        if(context!=null)
        for (editText in editTexts) {
            editText.clearFocus()
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(editText.windowToken, 0)
        }
    }


    fun setFocusListeners(vararg editTexts: TextInputEditText) {
        for (editText in editTexts) {
            editText.setOnFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    clearError(editText)
                }
            }
        }
    }


    fun clearErrors(vararg editTexts: TextInputEditText) {
        for (v in editTexts) {
            clearError(v)
        }
    }


    fun clearError(v: TextInputEditText) {
        var parent = v.parent
        while (parent is View) {
            if (parent is TextInputLayout) {
                parent.error = null
                break
            }
            parent = parent.getParent()
        }
    }


}
