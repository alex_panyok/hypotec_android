package hypotec.aws3.com.util

object QuizKeys {

    const val STEP_DATA_ST_1 = "step_data[step_1]"

    const val STEP_DATA_ST_2 = "step_data[step_2]"

    const val STEP_DATA_ST_2_M_B = "step_data[step_2_mortgage_balance]"

    const val STEP_DATA_ST_3 = "step_data[step_3]"

    const val STEP_DATA_ST_4 = "step_data[step_4]"

    const val STEP_DATA_ST_5 = "step_data[step_5]"

    const val STEP_DATA_ST_5_C_A = "step_data[step_5_cash_amount]"

    const val STEP_DATA_ST_5_F_P = "step_data[step_5_full_price]"

    const val STEP_DATA_ST_5_D_P = "step_data[step_5_down_payment]"

    const val STEP_DATA_ST_5_D_P_P = "step_data[step_5_down_payment_price]"

    const val STEP_DATA_ST_6 = "step_data[step_6]"

    const val STEP_DATA_ST_7 = "step_data[step_7]"

    const val STEP_DATA_ST_8 = "step_data[step_8]"

    const val STEP_DATA_ST_REC_OPTION = "step_data[recommend_option]"

    const val STEP_DATA_ST_SEL_OPTION= "step_data[selected_option]"

    const val POST_ID = "post_id"

    const val ACTION = "action"

    const val FIRST_NAME = "first_name"

    const val LAST_NAME = "last_name"

    const val EMAIL = "email"

    const val PHONE = "phone"

}