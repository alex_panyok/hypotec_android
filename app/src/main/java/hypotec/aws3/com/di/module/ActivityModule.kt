package hypotec.aws3.com.di.module

import android.app.Activity
import dagger.Binds
import dagger.Module
import dagger.android.ActivityKey
import dagger.android.AndroidInjector
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.CalculatorComponent
import hypotec.aws3.com.di.QuizComponent
import hypotec.aws3.com.view.activity.CalculatorActivity
import hypotec.aws3.com.view.activity.QuizActivity


@Module(includes = arrayOf(ViewModelModule::class))
abstract class ActivityModule {

    @Binds
    @IntoMap
    @ActivityKey(QuizActivity::class)
    abstract fun bindQuizActivityInjectorFactory(builder: QuizComponent.Builder): AndroidInjector.Factory<out Activity>



    @Binds
    @IntoMap
    @ActivityKey(CalculatorActivity::class)
    abstract fun bindCalcActivityInjectorFactory(builder: CalculatorComponent.Builder): AndroidInjector.Factory<out Activity>
}