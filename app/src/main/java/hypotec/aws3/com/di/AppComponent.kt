package hypotec.aws3.com.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import hypotec.aws3.com.app.HypotecApplication
import hypotec.aws3.com.di.module.ActivityModule
import hypotec.aws3.com.di.module.DataSourceModule
import hypotec.aws3.com.di.module.RepoModule
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, AndroidSupportInjectionModule::class, ActivityModule::class, RepoModule::class, DataSourceModule::class))
interface AppComponent {


    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: HypotecApplication): Builder

        fun build(): AppComponent
    }

    fun inject(app: HypotecApplication)
}