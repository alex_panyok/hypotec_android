package hypotec.aws3.com.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import hypotec.aws3.com.view.fragment.QuizCalcFragment
import hypotec.aws3.com.view.fragment.QuizInputFragment

@Subcomponent
interface QuizCalcComponent : AndroidInjector<QuizCalcFragment> {


    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<QuizCalcFragment>()
}