package hypotec.aws3.com.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.QuizListComponent
import hypotec.aws3.com.view.fragment.QuizListFragment

@Module(subcomponents = arrayOf(QuizListComponent::class))
abstract class QuizListFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(QuizListFragment::class)
    abstract fun bindQuizListFragmentInjectorFactory(builder: QuizListComponent.Builder): AndroidInjector.Factory<out Fragment>
}