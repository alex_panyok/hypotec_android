package hypotec.aws3.com.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import hypotec.aws3.com.di.module.CalculatorActivityModule
import hypotec.aws3.com.view.activity.CalculatorActivity


@Subcomponent(modules = arrayOf(CalculatorActivityModule::class))
interface CalculatorComponent : AndroidInjector<CalculatorActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<CalculatorActivity>()
}