package hypotec.aws3.com.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import hypotec.aws3.com.view.fragment.QuizListFragment
import hypotec.aws3.com.view.fragment.QuizSimpleFragment


@Subcomponent
interface QuizSimpleComponent : AndroidInjector<QuizSimpleFragment>  {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<QuizSimpleFragment>()
}