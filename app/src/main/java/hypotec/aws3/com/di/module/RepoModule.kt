package hypotec.aws3.com.di.module

import dagger.Binds
import dagger.Module
import hypotec.aws3.com.domain.repo.QuizRepo
import hypotec.aws3.com.domain.repo.QuizRepoImpl

@Module
abstract class RepoModule {

    @Binds
    abstract fun bindQuizRepo(repo: QuizRepoImpl): QuizRepo
}