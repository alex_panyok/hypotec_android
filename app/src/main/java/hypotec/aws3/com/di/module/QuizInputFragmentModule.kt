package hypotec.aws3.com.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.QuizInputComponent
import hypotec.aws3.com.di.QuizListComponent
import hypotec.aws3.com.view.fragment.QuizInputFragment
import hypotec.aws3.com.view.fragment.QuizListFragment

@Module(subcomponents = arrayOf(QuizInputComponent::class))
abstract class QuizInputFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(QuizInputFragment::class)
    abstract fun bindQuizListFragmentInjectorFactory(builder: QuizInputComponent.Builder): AndroidInjector.Factory<out Fragment>
}