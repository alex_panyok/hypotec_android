package hypotec.aws3.com.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import hypotec.aws3.com.di.module.*
import hypotec.aws3.com.view.activity.QuizActivity


@Subcomponent(modules = arrayOf(
        QuizActivityModule::class,
        QuizListFragmentModule::class,
        QuizInputFragmentModule::class,
        QuizCalcFragmentModule::class,
        QuizSimpleFragmentModule::class,
        QuizResultFragmentModule::class))
interface QuizComponent : AndroidInjector<QuizActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<QuizActivity>()

}