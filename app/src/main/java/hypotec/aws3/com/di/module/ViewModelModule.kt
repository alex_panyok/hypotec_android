package hypotec.aws3.com.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.ViewModelKey
import hypotec.aws3.com.view.fragment.QuizResultFragment
import hypotec.aws3.com.viewmodel.*


@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindCurrencyViewModel(mainViewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(QuizListViewModel::class)
    abstract fun bindQuizListViewModel(quizListViewModel: QuizListViewModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(QuizInputModel::class)
    abstract fun bindQuizInputViewModel(quizListViewModel: QuizInputModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(QuizCalcModel::class)
    abstract fun bindQuizCalcViewModel(quizListViewModel: QuizCalcModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(QuizSimpleModel::class)
    abstract fun bindQuizSimpleViewModel(quizSimpleViewModel: QuizSimpleModel): ViewModel


    @Binds
    @IntoMap
    @ViewModelKey(QuizResultModel::class)
    abstract fun bindQuizResultViewModel(quizResultModel: QuizResultModel): ViewModel



    @Binds
    @IntoMap
    @ViewModelKey(CalculatorModel::class)
    abstract fun bindCalcViewModel(calculatorModel: CalculatorModel): ViewModel
}