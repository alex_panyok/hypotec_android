package hypotec.aws3.com.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import hypotec.aws3.com.view.fragment.QuizResultFragment
import hypotec.aws3.com.view.fragment.QuizSimpleFragment

@Subcomponent
interface QuizResultComponent : AndroidInjector<QuizResultFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<QuizResultFragment>()
}