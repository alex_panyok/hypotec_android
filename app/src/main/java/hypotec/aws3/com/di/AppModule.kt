package hypotec.aws3.com.di

import android.content.Context
import dagger.Module
import dagger.Provides
import hypotec.aws3.com.app.HypotecApplication
import hypotec.aws3.com.di.module.RemoteModule
import javax.inject.Singleton


@Module(subcomponents = arrayOf(QuizComponent::class, CalculatorComponent::class), includes = arrayOf(RemoteModule::class))
class AppModule {

    @Provides
    @Singleton
    fun provideContext(application: HypotecApplication): Context {
        return application.applicationContext
    }
}