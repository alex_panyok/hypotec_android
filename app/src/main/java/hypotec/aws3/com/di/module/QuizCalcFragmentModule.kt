package hypotec.aws3.com.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.QuizCalcComponent
import hypotec.aws3.com.view.fragment.QuizCalcFragment

@Module(subcomponents = arrayOf(QuizCalcComponent::class))
abstract class QuizCalcFragmentModule {


    @Binds
    @IntoMap
    @FragmentKey(QuizCalcFragment::class)
    abstract fun bindQuizCalcFragmentInjectorFactory(builder: QuizCalcComponent.Builder): AndroidInjector.Factory<out Fragment>
}