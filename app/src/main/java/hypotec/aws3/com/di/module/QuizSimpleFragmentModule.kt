package hypotec.aws3.com.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.QuizSimpleComponent
import hypotec.aws3.com.view.fragment.QuizSimpleFragment

@Module(subcomponents = arrayOf(QuizSimpleComponent::class))
abstract class QuizSimpleFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(QuizSimpleFragment::class)
    abstract fun bindSimpleQuizFragmentInjectorFactory(builder: QuizSimpleComponent.Builder): AndroidInjector.Factory<out Fragment>
}