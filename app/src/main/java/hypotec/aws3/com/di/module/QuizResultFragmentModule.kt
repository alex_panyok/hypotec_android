package hypotec.aws3.com.di.module

import android.support.v4.app.Fragment
import dagger.Binds
import dagger.Module
import dagger.android.AndroidInjector
import dagger.android.support.FragmentKey
import dagger.multibindings.IntoMap
import hypotec.aws3.com.di.QuizResultComponent
import hypotec.aws3.com.di.QuizSimpleComponent
import hypotec.aws3.com.view.fragment.QuizResultFragment
import hypotec.aws3.com.view.fragment.QuizSimpleFragment

@Module(subcomponents = arrayOf(QuizResultComponent::class))
abstract class QuizResultFragmentModule {

    @Binds
    @IntoMap
    @FragmentKey(QuizResultFragment::class)
    abstract fun bindResultQuizFragmentInjectorFactory(builder: QuizResultComponent.Builder): AndroidInjector.Factory<out Fragment>
}