package hypotec.aws3.com.di

import dagger.Subcomponent
import dagger.android.AndroidInjector
import hypotec.aws3.com.view.fragment.QuizListFragment


@Subcomponent
interface QuizListComponent : AndroidInjector<QuizListFragment> {


    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<QuizListFragment>()
}