package hypotec.aws3.com.view.activity

import android.content.Intent
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.core.internal.deps.dagger.internal.Preconditions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import hypotec.aws3.com.R
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class QuizActivityTest {


    @get:Rule
    var mQuizActivityTestRule = ActivityTestRule<QuizActivity>(QuizActivity::class.java, true, false)


    @Test
    fun testToolbar() {
        onView(ViewMatchers.withId(R.id.toolbar)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }


    @Test
    fun testActionBar(){
        Preconditions.checkNotNull(mQuizActivityTestRule.activity.supportActionBar)
        onView(ViewMatchers.withId(R.id.action_call)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }


    @Test
    fun testProgressIsDisplayed(){
        onView(ViewMatchers.withId(R.id.progressSteps)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }


    @Before
    fun setUp() {
        mQuizActivityTestRule.launchActivity(Intent())
    }

    @After
    fun tearDown() {
    }

}