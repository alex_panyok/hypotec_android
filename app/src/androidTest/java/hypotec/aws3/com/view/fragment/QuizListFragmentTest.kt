package hypotec.aws3.com.view.fragment

import android.os.Bundle
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions
import android.support.test.espresso.matcher.ViewMatchers
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import hypotec.aws3.com.R
import hypotec.aws3.com.view.activity.QuizActivity
import kotlinx.android.synthetic.main.fragment_list_quiz.view.*
import org.junit.After
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class QuizListFragmentTest{


    @get:Rule
    var mQuizActivityTestRule = ActivityTestRule<QuizActivity>(QuizActivity::class.java, true, true)


    @Before
    fun setUp() {
        val fragment = QuizListFragment.create(Bundle())
        mQuizActivityTestRule.activity.setFragment(fragment, "tag")
    }


    @Test
    fun testUiIsShowing() {
        onView(ViewMatchers.withId(R.id.titleFragment)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
        onView(ViewMatchers.withId(R.id.recycleView)).check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @After
    fun tearDown() {

    }
}