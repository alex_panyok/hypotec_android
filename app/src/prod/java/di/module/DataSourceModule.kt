package hypotec.aws3.com.di.module

import dagger.Binds
import dagger.Module
import hypotec.aws3.com.domain.data.DataSource
import hypotec.aws3.com.domain.data.FakeQuizDataSource
import hypotec.aws3.com.domain.data.QuizDataSource


@Module
abstract class DataSourceModule {

    @Binds
    abstract fun bindDataSource(dataSource: QuizDataSource): DataSource
}