package hypotec.aws3.com.domain.data

import android.content.Context
import hypotec.aws3.com.R
import hypotec.aws3.com.domain.ListAnswerItem
import hypotec.aws3.com.domain.ListQuizItem
import hypotec.aws3.com.domain.QuizItem
import hypotec.aws3.com.enums.Quiz
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton


open class FakeQuizDataSource:DataSource {
    override fun getQuiz(quiz: Quiz): Observable<List<QuizItem>> {
        return Observable.just(QuizListUtil.genList())
    }




}