package hypotec.aws3.com.di.module

import dagger.Binds
import dagger.Module
import hypotec.aws3.com.domain.data.DataSource
import hypotec.aws3.com.domain.data.FakeQuizDataSource


@Module
abstract class DataSourceModule {

    @Binds
    abstract fun bindDataSource(dataSource: FakeQuizDataSource): DataSource
}