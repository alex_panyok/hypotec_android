package hypotec.aws3.com.domain.data

import hypotec.aws3.com.domain.QuizItem
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FakeQuizDataSource @Inject constructor() : DataSource {


    override fun getQuiz(): Observable<List<QuizItem>> {
        return Observable.just(QuizListUtil.genList())
    }

}