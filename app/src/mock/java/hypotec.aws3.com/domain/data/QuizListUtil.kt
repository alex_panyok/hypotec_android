package hypotec.aws3.com.domain.data

import hypotec.aws3.com.domain.ListAnswerItem
import hypotec.aws3.com.domain.ListQuizItem
import hypotec.aws3.com.domain.QuizItem


class QuizListUtil {


    companion object {
        fun genList(): List<QuizItem> {
            return listOf(ListQuizItem("TITLE", answers =
            listOf(ListAnswerItem(1, title = "TITLE"),
                    ListAnswerItem(1, title = "TITLE"),
                    ListAnswerItem(1, title = "TITLE"))),
                    ListQuizItem("TITLE", answers =
                    listOf(ListAnswerItem(1, title = "TITLE"))))

        }

    }
}