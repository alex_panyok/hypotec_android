package hypotec.aws3.com.domain.repo

import hypotec.aws3.com.domain.data.DataSource
import hypotec.aws3.com.domain.data.FakeQuizDataSource
import org.hamcrest.Matchers.isA
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test

class QuizRepoTest {

    lateinit var quizDatSource: DataSource


    @Before
    fun setUp() {
        quizDatSource = FakeQuizDataSource()

    }

    @Test
    fun getQuiz() {
        assertThat(quizDatSource.getQuiz().blockingFirst(), isA(List::class.java))
    }

}