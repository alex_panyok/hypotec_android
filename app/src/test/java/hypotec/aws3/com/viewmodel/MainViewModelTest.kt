package hypotec.aws3.com.viewmodel

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import hypotec.aws3.com.domain.QuizItem
import hypotec.aws3.com.domain.data.QuizListUtil
import hypotec.aws3.com.domain.repo.QuizRepo
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.hamcrest.Matchers
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class MainViewModelTest {


    @Mock
    lateinit var repo: QuizRepo

    lateinit var viewModel: MainViewModel

    @get:Rule
    val rule: TestRule = InstantTaskExecutorRule()


    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        MockitoAnnotations.initMocks(this)
        viewModel = MainViewModel(repo)
    }

    @Test
    fun getLiveDataQuiz() {
        assertThat(viewModel.liveDataQuiz, Matchers.`is`(Matchers.notNullValue()))
    }

    @Test
    fun getLiveDataCurrentQuiz() {
        assertThat(viewModel.liveDataCurrentQuiz, Matchers.`is`(Matchers.notNullValue()))
    }

    @Test
    fun getQuiz() {
        val list = QuizListUtil.genList()
        Mockito.`when`(repo.getQuiz()).thenReturn(Observable.just(list))
        val observer: Observer<List<QuizItem>> = Mockito.mock(Observer::class.java) as Observer<List<QuizItem>>
        viewModel.liveDataQuiz.observeForever(observer)
        viewModel.getQuiz()
        Mockito.verify(repo).getQuiz()
        Mockito.verify(observer).onChanged(list)
    }

    @Test
    operator fun next() {
        val list = QuizListUtil.genList()
        viewModel.liveDataQuiz.value = list
        val observer: Observer<QuizItem> = Mockito.mock(Observer::class.java) as Observer<QuizItem>
        viewModel.liveDataCurrentQuiz.observeForever(observer)
        viewModel.next()
        Mockito.verify(observer).onChanged(list[viewModel.index-1])
    }

}